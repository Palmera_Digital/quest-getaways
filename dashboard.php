
<? 
session_start();
if ($_SESSION['LoggedIn'] !== 'pineapple'){ ?><meta http-equiv="refresh" content="0; url=index.php" /><? }


?>
<!doctype html>
<html>
<head>

		<title>Quest - Powered by Palmera</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"/>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>

<link rel="apple-touch-icon" sizes="180x180" href="icons/apple-touch-icon.png">
<link rel="icon" type="image/png" href="icons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="icons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="icons/manifest.json">
<link rel="mask-icon" href="icons/safari-pinned-tab.svg" color="#3d4e25">


<meta name="theme-color" content="#ffffff">
		

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/anim.js"></script>

		

		<script type="text/javascript" src="js/jquery.infinitescroll.js"></script>

		<script type="text/javascript" src="js/manual-trigger.js"></script>

		<script type="text/javascript" src="js/svg4everybody.ie8.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/style_desktop.css"/>

		<link rel="stylesheet" type="text/css" href="css/style_responsive.css"/>

	<style>
		.questOption {
			font-family: source_r, Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
			min-height: 20vh;
			margin-top: 1em;
			margin-bottom: 1em;			
		}
		.questOptionInner {
			background: rgba(61,77,36,0.7);
			width: 100%;
		    height: 100% !important;
			padding: 20px;
		}
		.questOptionContent {
			position: relative;
		    top: 30%;
    		font-size: .9em;
			color: #fff;
		}
		.questOptionContent h3 {
			font-size: 32px;
		}
		.linkIcon {
			width: 100px;
		}
		#response {
			background: #fff;
		}
		.CTA {
    padding: 10px;
    background: #fff;
    border-radius: 10px;
    box-shadow: 2px 2px 7px;
    font-size: 18px;
    font-weight: bold;
	margin: 0 15px;
}
		@media only screen and (min-width : 768px) {
    .is-table-row {
        display: table;
    }
    .is-table-row [class*="col-"] {
        float: none;
        display: table-cell;
        vertical-align: top;
    }
}
	
	</style>
  <script>
  jQuery( function() {
    jQuery( "#tabContainer" ).tabs();
  } );
  </script>
  

	</head>
	
<?php
$ownerNumber = $_GET['number'];
$pid = $_GET['PID'];
$token = $_GET['token'];

//	$token = '4f19726d-c832-484f-a880-d175d1383016';
$str = '2002270-200485-'.$ownerNumber;
	$name = $_GET['firstName'] . " " . $_GET['lastName'];
	$status = $_GET['status'];
$pType = $_GET['pType'];
$extendLink = sha1($str);

	$showPassport = 0;

if ($pType == "Club.NoPkg")	{
	?>
	
<script>

	jQuery( document ).ready(function(){
console.log("Okay, we're running the scripts.");
			/* stop form from submitting normally */
			


			/* get the action attribute from the <form action=""> element */
			var url =  'https://www.vacationplay.com/palmeravacationclub/quest/passportbypid.php' ;
			var thePID = <? echo $pid; ?>;


			/* Send the data using post with element id name and name2*/
			var posting = jQuery.post( url, {
				pid:  thePID 
				
			} );
			if (thePID !== 0) {
			/* Alerts the results */
			posting.done( function ( data ) {


				var $responseData = data;
				console.log( "Data" + $responseData );
				var n = data.includes("problem");
				
				console.log(n);
				if (n != true) {
				var response = JSON.parse(data);
				 console.log("There is no problem, so N is not true");
							var content = response["content"];
							var link = response["url"];
						
					if (link) { var ctAction = "REDEEM YOUR PASSPORT BACK TO HILTON HEAD ISLAND!"; } else { var ctAction = "CALL TO SPEAK TO A RESERVATION AGENT"; }
				
					
				
						
					 $("#response").html('<div class="col-md-8" style="color: #fff; font-size: 125%;"><h3 class="beyond" style="color: #fff;font-size: 30px;letter-spacing: 1px;">Your Quest Hilton Head Island Passport</h3><p style="color: #fff;">' + content + '</p></div><div class="col-md-4" style="vertical-align: middle;"><a href="' + link + '" target="_blank"><div class="CTA" style="font-size: 85%">' + ctAction + '</div></div>');
						$("#passportTab").css('display', 'inline-block');
						$("tabs-3").css('display', 'inline-block');
				}
				
				console.log("Show passport: <? echo $showPassport; ?>")
				//self.parent.location.href = "reservation-confirmation.php?" + data;
			} );
			} else { console.log("PID is 0");}
		} );
		
	</script>
	
<? } ?>	
	
<body class="background_img_home" style="background-image:url(img/cover.jpg); background-attachment: fixed;">
	<div class="row" id="infoHeader">
			<div class="col-md-push-6 col-md-6 text-right" id='headerText'>
				<p><!--<img src="img/phone-icon.png" width="25">800-489-0760<br>-->
					<span style="margin-left:10px;font-size: 24px;">Welcome, <? echo $name; ?> | <a href="logout.php">Log Out</a></span><br>
					<a href="https://travel.questgetaways.com/vacationclub/logincheck.aspx?Token=<? echo $token; ?>&RedirectURL=%2Freservations%2F" target="_blank">My Account</a> | 
			<a href="https://travel.questgetaways.com/vacationclub/logincheck.aspx?Token=<? echo $token; ?>&RedirectURL=%2Fmembership%2Fsupport.aspx%2F" target="_blank">Support</a>
				</p>
			</div>
			<div class="col-md-pull-6 col-md-6" ><img src="img/QuestLogo_white palm.png" alt="Quest Travel Club" id="headerLogo"></div>
			
	</div>
	<div class="clearfix"></div>
	
	
	
	
	<div id="mainContainer">
	<!------------------------------------------------------------------>
	<!------------------------- PASSPORTS CTA -------------------------->
	<!------------------------------------------------------------------>
			
			<!--<li id="passportTab" style="display: none;"><a href="#tabs-3"><h2 class="beyond">My Passport</h2></a></li> -->
			<div id="tabs-3" class="container" >
		 		<div class="row-fluid text-center is-table-row" id="response" style="margin: 15px; background: url(img/Photo1-small.jpg); background-repeat:  repeat;">
		 			<!--<div class="col-xs-12"><a href="javascript:void(0)" target="_blank" onclick="window.open('https://www.mywebrez.com/questgetaways','booknow','scrollbar=yes, toolbar=yes, resizeable=yes,location=no,menubar=no,status=no')"><img src="img/Condos.png" alt="Condos" class="playLink"><br></a></div>-->
		 			
		 			
		 			
		 		</div>
			</div>

	<!------------------------------------------------------------------>
	<!---------------------- END PASSPORTS CTA ------------------------->
	<!------------------------------------------------------------------>
	
	
	
	
	<div class="container" id="" style="margin-bottom: 20vh;">
		<div class="row-fluid text-center" id="tabContainer" >
			<div class="col-md-3 col-sm-6 col-xs-12 questOption" id="hotels"><div class="questOptionInner"><a href="https://travel.questgetaways.com/vacationclub/logincheck.aspx?Token=<? echo $token; ?>&RedirectURL=%2Frentals%2F" target="_blank"><div class='questOptionContent'><img src="img/icons/hotels.png" class="linkIcon"><br><h3 class="beyond">Hotels</h3>Find great deals on hotels in your favorite vacation destinations.</div></div></a></div>
			
			
			<div class="col-md-3 col-sm-6 col-xs-12  questOption"><div class="questOptionInner"><a href="https://travel.questgetaways.com/vacationclub/logincheck.aspx?Token=<? echo $token; ?>&RedirectURL=%2Fcruises%2F" target="_blank"><div class='questOptionContent'><img src="img/icons/cruises.png" class="linkIcon"><br><h3 class="beyond">Cruises</h3>Discover special cruise deals on many of the world's best cruise lines.</div></div></a></div>
			
			<div class="col-md-3 col-sm-6 col-xs-12  questOption"><div class="questOptionInner"><a href="https://travel.questgetaways.com/vacationclub/logincheck.aspx?Token=<? echo $token; ?>&RedirectURL=%2Fcars%2F" target="_blank"><div class='questOptionContent'><img src="img/icons/cars.png" class="linkIcon"><br><h3 class="beyond">Cars</h3>What moves you? Grab a great deal on a rental car, and get away this weekend!</div></div></a></div>
			
			
			<div class="col-md-3 col-sm-6 col-xs-12  questOption"><div class="questOptionInner"><a href="https://travel.questgetaways.com/vacationclub/logincheck.aspx?Token=<? echo $token; ?>&RedirectURL=%2Fweeks%2F" target="_blank"><div class='questOptionContent'><img src="img/icons/weeks.png" class="linkIcon"><br><h3 class="beyond">Weeks</h3>Time for a week away? Your gateway to featured weekly stays all over the globe.</div></div></a></div>
			
			<div class="col-md-3 col-sm-6 col-xs-12  questOption" id="flights"><div class="questOptionInner"><a href="https://travel.questgetaways.com/vacationclub/logincheck.aspx?Token=<? echo $token; ?>&RedirectURL=%2Fflights%2F" target="_blank"><div class='questOptionContent'><img src="img/icons/flights.png" class="linkIcon"><br><h3 class="beyond">Flights</h3>Don't just stay in one place.  Grab an incredible deal on a multi-destination tour. </div></div></a></div>
			
			<div class="col-md-3 col-sm-6 col-xs-12  questOption" id="tourPackages"><div class="questOptionInner"><a href="https://travel.questgetaways.com/vacationclub/logincheck.aspx?Token=<? echo $token; ?>&RedirectURL=%2Ftours%2F" target="_blank"><div class='questOptionContent'><img src="img/icons/tours.png" class="linkIcon"><br><h3 class="beyond">Tours</h3>Don't just stay in one place.  Grab an incredible deal on a multi-destination tour. </div></div></a></div>
			
			<div class="col-md-3 col-sm-6 col-xs-12  questOption"><div class="questOptionInner"><a href="https://travel.questgetaways.com/vacationclub/logincheck.aspx?Token=<? echo $token; ?>&RedirectURL=%2Factivities%2F" target="_blank"><div class='questOptionContent'><img src="img/icons/activities.png" class="linkIcon"><br><h3 class="beyond">Activities</h3>Time for adventure? Find deals on excursions, theme park tickets, and more!</div></div></a></div>
			
			<div class="col-md-3 col-sm-6 col-xs-12  questOption"><div class="questOptionInner"><a href="https://www.mywebrez.com/questgetaways" target="_blank"><div class='questOptionContent'><img src="img/icons/rci-weeks.png" class="linkIcon"><br><h3 class="beyond">Weeks+</h3>Find even more weekly resort getaways through this Quest exclusive feature.</div></div></a></div>
			
		 	
		</div>
		<div class="clearfix"></div>

	</div>
</div>		
	<footer>
		<div class="row text-center">
			<div class="col-sm-12">
				Quest Getaways<br>
				33 Office Park Road, Suite 220 | Hilton Head Island, SC 29928<br>
				Customer Service: <a href="tel:8004890760">800-489-0760</a> | Email: <a href="mailto:customercare@questgetaways.com">customercare@questgetaways.com</a>
			</div>
		</div>
	</footer>
	
             
</body>
	<script>
		
	
		
		
		$(function() {
    console.log( "ready!" );
		
		var url = 'https://www.vacationplay.com/palmeravacationclub/quest/passportbypid.php';
		
		var posting = jQuery.post( url, {
				pid: <? echo $pid; ?>,
				
			} );
		
		posting.done( function ( data ) {


				var $responseData = data;	
			console.log( data );
				
				//self.parent.location.href = "reservation-confirmation.php?" + data;
			} );
});
		
		var mainContainerheight = jQuery("#infoHeader").height() + 100;
		console.log(mainContainerheight);
		jQuery("#mainContainer").css("margin-top", mainContainerheight + "px");
		
		var boxHeight = jQuery("#hotels").height();
		console.log("Box Height: " + boxHeight);
		jQuery(".questOptionInner").css("min-height", boxHeight + "px");
	</script>
</html>
