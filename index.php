<!DOCTYPE html>
<? session_start(); 

 if ($_SESSION['attempts'] < time()) { $_SESSION['attempts'] = 0; }

?>


<html>

	<head>

		<title>Quest - Powered by Palmera</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		<link rel="stylesheet" type="text/css" href="css/style_desktop_min.css"/>

		


<link rel="apple-touch-icon" sizes="180x180" href="icons/apple-touch-icon.png">
<link rel="icon" type="image/png" href="icons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="icons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="icons/manifest.json">
<link rel="mask-icon" href="icons/safari-pinned-tab.svg" color="#3d4e25">


<meta name="theme-color" content="#ffffff">
		<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>

		<script type="text/javascript" src="js/anim.js"></script>



	</head>

	<body>

		
	

		<!-- LOADER -->

		<div class="loader" >
			<div class="point_loader_style point_loader_1"></div>
			<div class="point_loader_style point_loader_2"></div>
			<div class="point_loader_style point_loader_3"></div>
		</div>

		<!-- END LOADER -->


		<!-- LIGHTBOX PHOTO -->

		<div class="lightbox">

			<div class="close_button">
				<svg viewBox="0 0 19 19">
					<g id="cross" stroke-width="1" fill-rule="evenodd">
					    <g transform="translate(-1359.000000, -54.000000)">
					        <g transform="translate(915.000000, 29.000000)">
					            <g transform="translate(453.500000, 32.500000) rotate(-180.000000) translate(-453.500000, -32.500000) translate(422.000000, 1.000000)">
					                <path d="M43.3,29.5018 L32.5,29.5018 L32.5,40.3018 C32.5,40.7986 32.095,41.2 31.6,41.2 C31.1032,41.2 30.7,40.7986 30.7,40.3018 L30.7,29.5018 L19.9,29.5018 C19.4032,29.5018 19,29.0986 19,28.6 C19,28.105 19.4032,27.7 19.9,27.7 L30.7,27.7 L30.7,16.9 C30.7,16.405 31.1032,16 31.6,16 C32.095,16 32.5,16.405 32.5,16.9 L32.5,27.7 L43.3,27.7 C43.795,27.7 44.2,28.105 44.2,28.6 C44.2,29.0986 43.795,29.5018 43.3,29.5018" transform="translate(32.500000, 29.500000) rotate(-45.000000) translate(-32.500000, -29.500000) "></path>
					            </g>
					        </g>
					    </g>
					</g>
				</svg>
			</div>

			<div class="nav_lightbox">
				<div class="top_button">
					<svg viewBox="0 0 12 25">
					    <g stroke="none" stroke-width="1" fill-rule="evenodd">
					        <g transform="translate(-709.000000, -764.000000)" >
					            <g id="Cover">
					                <path d="M713.090539,764.082981 L713.090539,784.290746 L710.499434,781.793664 C710.288386,781.591514 709.945645,781.591514 709.734597,781.793664 L708.966945,782.522629 C708.755898,782.722552 708.755898,783.048887 708.966945,783.251594 L713.791218,787.889907 C714.004517,788.089273 714.347822,788.089273 714.55887,787.889907 L719.356128,783.279439 C719.569427,783.078402 719.569427,782.752067 719.356128,782.55103 L718.588477,781.822622 C718.377429,781.620472 718.032999,781.620472 717.821951,781.822622 L715.231972,784.31859 L715.231972,764.082981 C715.231972,763.787831 714.99391,763.551711 714.696754,763.551711 L713.626319,763.551711 C713.331415,763.551711 713.090539,763.787831 713.090539,764.082981" transform="translate(714.718000, 776.081672) rotate(-180.000000) translate(-714.718000, -776.081672) "></path>
					            </g>
					        </g>
					    </g>
					</svg>
				</div>
				<div class="bottom_button">
					<svg viewBox="0 0 12 26">
					    <g>
							<path fill-rule="evenodd" d="M 5.09 1.08 L 5.09 21.29 L 2.5 18.79 C 2.29 18.59 1.95 18.59 1.73 18.79 L 0.97 19.52 C 0.76 19.72 0.76 20.05 0.97 20.25 L 5.79 24.89 C 6 25.09 6.35 25.09 6.56 24.89 L 11.36 20.28 C 11.57 20.08 11.57 19.75 11.36 19.55 L 10.59 18.82 C 10.38 18.62 10.03 18.62 9.82 18.82 L 7.23 21.32 L 7.23 1.08 C 7.23 0.79 6.99 0.55 6.7 0.55 L 5.63 0.55 C 5.33 0.55 5.09 0.79 5.09 1.08"></path>
						</g>
					</svg>
				</div>		
			</div>

			
		</div>

		<!-- END LIGHTBOX PHOTO -->


		<!-- ACTIVE MENU 

		<div class="button_active_menu">
			<div class="bar_burger_style bar_burger_1"></div>
			<div class="bar_burger_style bar_burger_2"></div>
			<div class="bar_burger_style bar_burger_3"></div>
		</div>

		<!-- END ACTIVE MENU -->


		<!-- SHARE CONTENT 

		<div class="container_share_content">
			<div class="share_header">
				<svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 14">
				    <path fill-rule="evenodd" d="M 17.4 1.75 C 17.28 1.62 17.1 1.59 16.94 1.66 C 16.85 1.71 16.67 1.78 16.46 1.84 C 16.73 1.48 16.94 1.09 17.04 0.73 C 17.08 0.57 17.02 0.41 16.89 0.32 C 16.76 0.23 16.58 0.23 16.45 0.32 C 16.25 0.46 15.25 0.89 14.62 1.03 C 13.21 -0.2 11.53 -0.33 9.82 0.66 C 8.42 1.48 8.12 3.14 8.18 4.15 C 4.98 3.84 2.99 2.15 1.87 0.75 C 1.79 0.66 1.66 0.6 1.55 0.61 C 1.42 0.62 1.31 0.69 1.24 0.8 C 0.71 1.69 0.57 2.68 0.82 3.66 C 0.96 4.19 1.2 4.66 1.48 5.04 C 1.35 4.97 1.22 4.89 1.1 4.79 C 0.98 4.7 0.83 4.68 0.69 4.74 C 0.56 4.81 0.48 4.94 0.48 5.09 C 0.48 6.83 1.57 7.91 2.53 8.47 C 2.36 8.45 2.19 8.41 2.01 8.36 C 1.87 8.31 1.71 8.36 1.62 8.47 C 1.52 8.58 1.49 8.73 1.56 8.87 C 2.11 10.09 3.15 10.91 4.44 11.22 C 3.32 11.87 1.84 12.18 0.42 12.02 C 0.24 12 0.07 12.07 0.02 12.24 C -0.04 12.41 0.04 12.45 0.19 12.53 C 2.07 13.59 3.96 13.88 5.83 13.88 L 5.83 13.88 C 8.62 13.88 11.24 12.92 13.19 10.73 C 15.02 8.68 15.98 6.27 15.79 3.9 C 16.27 3.54 16.99 2.9 17.44 2.19 C 17.53 2.05 17.52 1.87 17.4 1.75" />
				</svg>
			</div>
			<div class="share_header">
				<svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 20">
				    <path fill-rule="evenodd" d="M 9.02 3.33 L 9.94 3.33 C 10.6 3.33 10.67 2.87 10.67 2.64 L 10.67 0.64 C 10.67 0.41 10.6 0 9.94 0 L 8.06 0 C 5.6 0 3.34 1.94 3.34 4.18 L 3.34 7.33 L 0.6 7.33 C -0.06 7.33 0 7.75 0 7.98 L 0 10.64 C 0 10.87 -0.06 11.33 0.6 11.33 L 3.34 11.33 L 3.34 19.38 C 3.34 19.79 3.27 20 3.94 20 L 5.94 20 C 6.6 20 6.67 19.73 6.67 19.31 L 6.67 11.33 L 9.49 11.33 C 9.68 11.33 9.85 11.03 9.9 10.86 L 10.86 8.1 C 10.89 7.97 10.87 7.72 10.79 7.62 C 10.71 7.51 10.59 7.33 10.46 7.33 L 6.67 7.33 L 6.67 4.9 C 6.67 4.15 8.11 3.33 9.02 3.33" />
				</svg>
			</div>
		</div>

		<!-- END SHARE CONTENT -->

		
		<!-- MENU 

		<div class="container_menu">
			<div class="menu">
				<div class="content_menu maven_b">
					<div class="container_on_menu">
						<div class="content_on_menu content_on_menu_1">
							HOME
						</div>
					</div>
					<div class="container_on_menu">
						<div class="content_on_menu content_on_menu_2">
							ABOUT
						</div>
					</div>
					<div class="container_on_menu">
						<div class="content_on_menu content_on_menu_3">
							STORY
						</div>
					</div>
					<div class="container_on_menu">
						<div class="content_on_menu content_on_menu_3">
							<div class="share_menu">
								<svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 14">
								    <path fill-rule="evenodd" d="M 17.4 1.75 C 17.28 1.62 17.1 1.59 16.94 1.66 C 16.85 1.71 16.67 1.78 16.46 1.84 C 16.73 1.48 16.94 1.09 17.04 0.73 C 17.08 0.57 17.02 0.41 16.89 0.32 C 16.76 0.23 16.58 0.23 16.45 0.32 C 16.25 0.46 15.25 0.89 14.62 1.03 C 13.21 -0.2 11.53 -0.33 9.82 0.66 C 8.42 1.48 8.12 3.14 8.18 4.15 C 4.98 3.84 2.99 2.15 1.87 0.75 C 1.79 0.66 1.66 0.6 1.55 0.61 C 1.42 0.62 1.31 0.69 1.24 0.8 C 0.71 1.69 0.57 2.68 0.82 3.66 C 0.96 4.19 1.2 4.66 1.48 5.04 C 1.35 4.97 1.22 4.89 1.1 4.79 C 0.98 4.7 0.83 4.68 0.69 4.74 C 0.56 4.81 0.48 4.94 0.48 5.09 C 0.48 6.83 1.57 7.91 2.53 8.47 C 2.36 8.45 2.19 8.41 2.01 8.36 C 1.87 8.31 1.71 8.36 1.62 8.47 C 1.52 8.58 1.49 8.73 1.56 8.87 C 2.11 10.09 3.15 10.91 4.44 11.22 C 3.32 11.87 1.84 12.18 0.42 12.02 C 0.24 12 0.07 12.07 0.02 12.24 C -0.04 12.41 0.04 12.45 0.19 12.53 C 2.07 13.59 3.96 13.88 5.83 13.88 L 5.83 13.88 C 8.62 13.88 11.24 12.92 13.19 10.73 C 15.02 8.68 15.98 6.27 15.79 3.9 C 16.27 3.54 16.99 2.9 17.44 2.19 C 17.53 2.05 17.52 1.87 17.4 1.75" />
								</svg>
							</div>
							<div class="share_menu">
								<svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 20">
								    <path fill-rule="evenodd" d="M 9.02 3.33 L 9.94 3.33 C 10.6 3.33 10.67 2.87 10.67 2.64 L 10.67 0.64 C 10.67 0.41 10.6 0 9.94 0 L 8.06 0 C 5.6 0 3.34 1.94 3.34 4.18 L 3.34 7.33 L 0.6 7.33 C -0.06 7.33 0 7.75 0 7.98 L 0 10.64 C 0 10.87 -0.06 11.33 0.6 11.33 L 3.34 11.33 L 3.34 19.38 C 3.34 19.79 3.27 20 3.94 20 L 5.94 20 C 6.6 20 6.67 19.73 6.67 19.31 L 6.67 11.33 L 9.49 11.33 C 9.68 11.33 9.85 11.03 9.9 10.86 L 10.86 8.1 C 10.89 7.97 10.87 7.72 10.79 7.62 C 10.71 7.51 10.59 7.33 10.46 7.33 L 6.67 7.33 L 6.67 4.9 C 6.67 4.15 8.11 3.33 9.02 3.33" />
								</svg>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- END MENU -->
		
		
		<!-- HOME -->

		<div class="home" >

			<div class="rectangle_blanc_background_anim" style="b"></div>

			<div class="container_svg_round_img">
				<svg class="svg_round_img" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 1001 78" enable-background="new 0 0 1001 78" xml:space="preserve">
				<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFF" d="M1001,0H0v78C0,78,81.3,1.1,498.9,1.1S1001,78,1001,78V0z"/>
				</svg>
			</div>
			<div class="container_background_img_home" style="background:url(img/header-01-01-01.jpg)">
				<div class="background_img_home" style="background-image:url(img/cover.jpg)"></div><!-- CHANGE HOME PIC HERE -->
			</div>

			<div class="container_text_home maven_b">
				<div class="title_home"><img src="img/QuestLogo_GREEN.png"></div> <!-- 18 CHAR MAX -->
				<div class="subtitle_home">&nbsp;<br>
					<? if ($_SESSION['attempts'] < time()) { ?>
					<form action="check-account.php" method="post"><input class="mainLogin" type="email" id="emailAddress" name="emailAddress" placeholder="Email Address"><input name="Login" class="mainLogin" type="submit" id="Login" value="Log In"></form>
				<? } ?>
				</div>

				<div class="container_title_home_slice">
					<div class="title_home_slice_1">
						<div class="text_title_home_slice">
							<img src="img/QuestLogo_WHITE.png">
						</div>	
					</div>
					<div class="bar_title_home_slice"></div>
					<div class="title_home_slice_2">
						<div class="text_title_home_slice">
							<img src="img/QuestLogo_WHITE.png">
						</div>
					</div>
				</div>
			</div>

			<a href="#scroll">
                       
				<div class="slide_home">

					<svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 26">
					    <path fill-rule="evenodd" d="M 5.09 1.08 L 5.09 21.29 L 2.5 18.79 C 2.29 18.59 1.95 18.59 1.73 18.79 L 0.97 19.52 C 0.76 19.72 0.76 20.05 0.97 20.25 L 5.79 24.89 C 6 25.09 6.35 25.09 6.56 24.89 L 11.36 20.28 C 11.57 20.08 11.57 19.75 11.36 19.55 L 10.59 18.82 C 10.38 18.62 10.03 18.62 9.82 18.82 L 7.23 21.32 L 7.23 1.08 C 7.23 0.79 6.99 0.55 6.7 0.55 L 5.63 0.55 C 5.33 0.55 5.09 0.79 5.09 1.08" />
					</svg>
				</div>
			</a>
		</div>

		<!-- END HOME -->


		<!-- BOX -->

		<a id="scroll"></a>

		<div class="container_box" id="content">
			<div class="box_style_2 anim_box_1">
				<!-- <div class="box_in_style_1" style="background-image: url(img/Photo1-small.jpg);">
					<div class="content_box_in">
						<div class="logo_content_box_in">
							<svg viewBox="0 0 50 40" style="width: 15%;" fill="#aeb0ae" >
								<use xlink:href="svg/twitter.svg#twitter"></use>
							</svg>
						</div>
						<div class="title_content_box_in maven_b" style="color: #aeb0ae;">
							TWITTER
						</div>
						<div class="info_content_box_in source_r">
							<!-- ADD MORE TEXT HERE 
						</div>
						<a href="" class="link_content_box_in source_r" style="color: #6a7edb;">Follow me</a>
					</div>
				</div> -->
                <div class="box_in_style_3" style="background-image: url(img/Photo1-small.jpg); background-color:rgba(255,255,255,0.5);">
					<div class="title_box_in_style_3 beyond" style="color: #fff;">
						Quest Travel Club
					</div>
					<div class="subtitle_box_in_style_3 source_r" style="color: #fff;">
						Quest  a premium benefit allowing private  access  to  a  full-service  travel  agency offering all of the essentials for a great vacation! Our  exclusively  designed  travel  and  leisure programs are the perfect choice for your next trip to paradise. Quest delivers value at any level, and offering many famous  brand name resorts at prices below the public travel websites.
					</div>
				</div>
                <div class="box_in_style_1 photo_min" data-photo="photo_1" style="background-image: url(img/Quest-Large-1.jpg);">
					<div class="overlay_photo_min">
						<div class="container_like">
							<div class="container_svg_like">
								<svg viewBox="0 0 22 20">
						    		<use xlink:href="svg/like.svg#like"></use>
								</svg>
							</div>
							<div class="compteur_like source_s">
								
							</div>
						</div>

						<div class="container_info_photo_min">
							<div class="title_photo_min beyond">
								Hotels
							</div>
							<div class="subtitle_photo_min source_r">
								Search and book thousands of great family hotels worldwide, all at member-only rates that consistently to beat publicly advertised rates.
							</div>
							
						</div>
					</div>
				</div><div class="box_in_style_1 photo_min"  data-photo="photo_2" style="background-image: url(img/Quest-image-1.jpg);">
					<div class="overlay_photo_min">
						<div class="container_like">
							<div class="container_svg_like">
								<svg viewBox="0 0 22 20">
						    		<use xlink:href="svg/like.svg#like"></use>
								</svg>
							</div>
							<div class="compteur_like source_s">
								
							</div>
						</div>

						<div class="container_info_photo_min">
							<div class="title_photo_min beyond">
								Worldwide Tours
							</div>
							<div class="subtitle_photo_min source_r">
								Featuring  once  in  a  lifetime  types  of  exotic vacations, all at spectacular member-only prices.
							</div>
							
						</div>
					</div>
				</div>
			</div><!-- WHITE SPACE 

			--><div class="photo_min box_style_2 anim_box_2" data-photo="photo_3" style="background-image: url(img/Quest-Large-9.jpg);">
				<div class="overlay_photo_min">
					<div class="container_like">
						<div class="container_svg_like">
							<svg viewBox="0 0 22 20">
					    		<use xlink:href="svg/like.svg#like"></use>
							</svg>
						</div>
						<div class="compteur_like source_s">
							
						</div>
					</div>

					<div class="container_info_photo_min">
						<div class="title_photo_min beyond">
							Resorts
						</div>
						<div class="subtitle_photo_min source_r">
							Search  and  book  from  among  thousands of full week stays in luxury villas and resorts worldwide.
						</div>
					
					</div>
				</div>
			</div><!-- WHITE SPACE 

			--><div class="photo_min box_style_2 anim_box_3" data-photo="photo_4" style="background-image: url(img/Quest-Large-2.jpg);">
				<div class="overlay_photo_min">
					<div class="container_like">
						<div class="container_svg_like">
							<svg viewBox="0 0 22 20">
					    		<use xlink:href="svg/like.svg#like"></use>
							</svg>
						</div>
						
					</div>

					<div class="container_info_photo_min">
						<div class="title_photo_min beyond">
							Shopping & Dining
						</div>
						<div class="subtitle_photo_min source_r">
							It’s not just where you go, it’s also what you do when you get there. Quest offers access to America’s Largest Private Discount Network, featuring over 300,000 of the world’s best brands for shopping, dining, amusement parks, and much, much more.
						</div>
						
					</div>
				</div>
			</div><!-- WHITE SPACE 

			--><div class="box_style_1 anim_box_4">
				<div class="photo_min box_in_style_1" data-photo="photo_5" style="background-image: url(img/Quest-Large-3.jpg);">
					<div class="overlay_photo_min">
						<div class="container_like">
							<div class="container_svg_like">
								<svg viewBox="0 0 22 20">
						    		<use xlink:href="svg/like.svg#like"></use>
								</svg>
							</div>
							
						</div>

						<div class="container_info_photo_min">
							<div class="title_photo_min beyond">
								Cruises
							</div>
							<div class="subtitle_photo_min source_r">
								Discover the fun with our selection of top cruise lines. Use our booking engine to book any cruise, anytime,  anywhere,  and  at  member-only  rates that can’t be beaten.
							</div>
							
						</div>
					</div>
				</div><div class="box_in_style_1" style="background: #3d4e25;">
					<div class="content_box_in" style="top:40%;">
						<div class="logo_content_box_in">
							
						</div>
                     
                        <form action="check-account.php"method="post" class="midPageLogin">
						  <div class="title_content_box_in beyond" style="color: #fff;  ">
                        Member Login
							
						</div>
                        
                        <? if ($_SESSION['attempts'] < time()) { ?>
						<div class="info_content_box_in source_r" style="color:#868985; width:100%;">
						<input type="email" placeholder="Email Address" name="emailAddress">
                        
                        	<input name="Login" type="submit" id="Login2" value="Log In">
						</div>
					</form>
					<? } ?>
					</div>
				</div><div class="box_in_style_3" style="background-image: url(img/Photo1-small.jpg);">
					<div class="title_box_in_style_3 beyond" style="color: #fff;">
						Life Is About Experiences
					</div>
					<div class="subtitle_box_in_style_3 source_r" style="color: #fff;">
						Isn’t it time you and your family experienced what it’s like to vacation in spacious Resort Condos that include 1, 2 & 3 bedrooms? Some are over 1,000 square feet, and many have full kitchens, washers & dryers, microwave ovens, lavish resort pools, and more. We also believe in saying YES! YES it’s available, and YES we have it at the lowest price you’re going to find anywhere...  Guaranteed!  Put  it  all  together, and you get the very best Resort Condo program anywhere.
					</div>
				</div>
			</div><!-- WHITE SPACE 

			--><div class="container_trio">
				<div class="element_trio_style" style="background: #3d4e25;">
					<div class="content_element_trio" style="top:40%;">
						<div class="logo_element_trio" style="fill: #FFF;">
							
						</div>
					       <div class="title_element_trio beyond" style="color: #fff;">
							Contact Us
						</div>
						<div class="subtitle_box_in_style_3 source_r" style="color: #fff; min-width: 422px;;">
                        	
                            <p>Customer Service: 800-489-0760</p>
                            <p>Email: customercare@questgetaways.com</p>
                            <p>Physical Address: 33 Office Park Road, Suite 220<br>Hilton Head Island, SC  29928</p>
                     </div>
                     
					</div>
				</div><div class="photo_min element_trio_style" data-photo="photo_6" style="background-image: url(img/Quest-Large-4.jpg);">
					<div class="content_box_in">
						<div class="container_like">
							<div class="container_svg_like">
								
							</div>
                      
							<div class="compteur_like source_s">
							
							</div>
						</div>

						<div class="container_info_photo_min">
							<div class="title_photo_min beyond">
								
							</div>
							
						</div>
					</div>
				</div><div class="element_trio_style" style="background-image: url(img/Quest-Large-8.jpg);">
					<div class="content_element_trio" style="top:40%;">
						<div class="logo_element_trio">
							
						</div>
                          
                            <form action="" method="post"  class="midPageLogin">
						  <div class="title_content_box_in beyond" style="color: #fff;     font-size: 50px;">
                        Member Login
							
						</div>
                        
                       <? if ($_SESSION['attempts'] < time()) { ?>
						<div class="info_content_box_in source_r" style="color:#868985; width:100%;">
						<input name="emailAddress" type="email" required="required" placeholder="Email Address">
                        	<input name="Login" type="submit" id="Login3" value="Log In">
						</div>
					</form>
					<? } ?>	
						
					</div>
				</div>
			</div><!-- WHITE SPACE 

			-->

			
            <!-- WHITE SPACE 

			--><div class="box_style_4">
				<div class="photo_min box_in_style_1" data-photo="photo_9" style="background-image: url(img/Quest-Large-7.jpg);">
					<div class="content_box_in">
						<div class="container_like">
							<div class="container_svg_like">
								
							</div>
							
						</div>

			
					</div>
				</div><div class="box_in_style_1" style="background-image: url(img/Quest-Large-5.jpg);">
					<div class="content_box_in">
						<div class="logo_content_box_in">
					
						</div>
					
					</div>
				</div>
			</div><!-- WHITE SPACE 

			--><div class="box_style_4">
				<div class="photo_min box_in_style_1" data-photo="photo_10" style="background-image: url(img/Quest-Large-10.jpg);">
					<div class="content_box_in">
						<div class="container_like">
							<div class="container_svg_like">
								
							</div>
							
						</div>

					
					</div>
				</div><div class="box_in_style_1 photo_min" data-photo="photo_11" style="background-image: url(img/Quest-Large-6.jpg);">
					<div class="content_box_in">
						<div class="container_like">
							<div class="container_svg_like">
							
							</div>
							
						</div>

					
					</div>
				</div>
			</div>
		</div>

		<a id="next" href="index2.html"></a>

		<!-- END BOX -->


		<!-- ABOUT US -->

		<div class="container_about_us">

			<div class="container_top_about_us" style="background-image:url(img/bitmap.png)"></div>
			<div class="shadow_about_us"></div>
			<div class="content_about_us">
				<div class="container_text_about_us">
					<div class="title_text_about maven_b">
						ABOUT US
					</div>
					<div class="block_text">
						<div class="title_block_text maven_b">
							OUR COMPANY
						</div>
						<div class="bar_block_text"></div>
						<div class="content_block_text source_r">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
						</div>
					</div>
					<div class="block_text">
						<div class="title_block_text maven_b">
							OUR PHILOSOPHY
						</div>
						<div class="bar_block_text"></div>
						<div class="content_block_text source_r">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						</div>
					</div>
					<div class="block_text">
						<div class="title_block_text maven_b">
							HOW WE WORKS
						</div>
						<div class="bar_block_text"></div>
						<div class="content_block_text source_r">
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.
						</div>
					</div>

					<div class="position_container_letstalk">
						<div class="container_letstalk">
							<div class="content_letstalk source_r">
								Let’s Talk
								<div class="bar_letstalk"></div>
							</div>
						</div>
					</div>
					
				</div>
			</div>

			<footer>
				<div class="content_footer source_r">
					<div class="info_footer">
						info@company.com
					</div>
					<div class="round_info"></div>
					<div class="info_footer">
						2677 Lexington Avenue, San Francisco CA
					</div>
					<div class="round_info"></div>
					<div class="info_footer">
						+1 415 712 444
					</div>
				</div>

				<div class="copyright source_r">
					Copyright Company
				</div>
			</footer>
			<div class="top_about_us">
				
			</div>
		</div>

		<!-- END ABOUT US -->


		<!-- STORY -->

		<div class="container_story">
			<div class="top_story" style="background-image:url(img/bitmap_2.png)"></div>
			<div class="shadow_story"></div>
			<div class="title_content_story maven_b">
				OUR COMPANY
				<div class="bar_title_content_story"></div>
			</div>
			<div class="content_story source_r">
				<div class="block_text_story">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					<br>
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
					<br>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
					<br>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					<br>
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
				</div>
				<div class="block_text_story">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					<br>
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
					<br>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					<br>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.  
				</div>
			</div>

			<footer>
				<div class="content_footer source_r">
					<div class="info_footer">
						info@company.com
					</div>
					<div class="round_info"></div>
					<div class="info_footer">
						2677 Lexington Avenue, San Francisco CA
					</div>
					<div class="round_info"></div>
					<div class="info_footer">
						+1 415 712 444
					</div>
				</div>

				<div class="copyright source_r">
					Copyright Company
				</div>
			</footer>
		</div>

		<!-- END STORY -->
<link rel="stylesheet" type="text/css" href="css/style_responsive.css"/>
		<script type="text/javascript" src="js/box.js"></script>
				<script type="text/javascript" src="js/jquery.min.js"></script>

		<script type="text/javascript" src="js/jquery.infinitescroll.js"></script>

		<script type="text/javascript" src="js/manual-trigger.js"></script>

		<script type="text/javascript" src="js/svg4everybody.ie8.min.js"></script>
	</body>

</html>