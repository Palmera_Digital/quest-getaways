-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 24, 2018 at 01:30 PM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Quest-Users`
--

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `Index` int(100) NOT NULL,
  `Number` varchar(10) NOT NULL,
  `Status` varchar(10) NOT NULL,
  `FullName` varchar(100) NOT NULL,
  `First Name` varchar(100) NOT NULL,
  `Last Name` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `PID` int(11) NOT NULL,
  `Level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`Index`, `Number`, `Status`, `FullName`, `First Name`, `Last Name`, `Email`, `PID`, `Level`) VALUES
(99, '4-12345', 'Open', '', 'Peter', 'Parker', 'spiderman@gmail.com', 9876543, 1),
(98, '4-23456', 'Open', '', 'Bruce', 'Banner', 'hulkingout@gmail.com', 8765432, 1),
(97, '4-34567', 'Open', '', 'Danny', 'Rand', 'immortalironfist@yahoo.com', 7654321, 1),
(96, '4-45678', 'Open', '', 'Luke', 'Cage', 'heroforhire@yahoo.com', 6543210, 1),
(95, '4-56789', 'Open', '', 'Jessica', 'Jones', 'jjones@privateeye.net', 5432109, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`Index`),
  ADD UNIQUE KEY `Index` (`Index`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `Index` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3506;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
